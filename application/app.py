import ast
import os

import boto3
from chalice import Chalice, CognitoUserPoolAuthorizer
from chalicelib.responses import NO_CONTENT

app = Chalice(app_name='application')
dynamodb = boto3.resource('dynamodb')

TABLE_NAME = os.environ.get('APP_TABLE_NAME')
PRIMARY_KEY = os.environ.get('TABLE_PK')
SORT_KEY = os.environ.get('TABLE_SK')

dynamodb_table = dynamodb.Table(TABLE_NAME)

authorizer = CognitoUserPoolAuthorizer(
    'PoolAuthorizer', provider_arns=[os.environ.get('COGNITO_USERPOOL_ID')]
)


@app.route('/users', methods=['POST'], authorizer=authorizer)
def create_user():
    request = get_body(app.current_request)
    item = {
        PRIMARY_KEY: request['username'],
        SORT_KEY: request['username'],
    }
    item.update(request)
    dynamodb_table.put_item(Item=item)
    return NO_CONTENT


@app.route('/users/{username}', methods=['GET'], authorizer=authorizer)
def get_user(username):
    key = {
        PRIMARY_KEY: username,
        SORT_KEY: username,
    }
    item = dynamodb_table.get_item(Key=key)['Item']
    del item[PRIMARY_KEY]
    del item[SORT_KEY]
    return item


def get_body(request):
    return request.json_body or ast.literal_eval(request.raw_body.decode('utf-8'))
